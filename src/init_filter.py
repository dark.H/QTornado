from initial_content import InitContent

from sys import version_info
if version_info[0] == 2:
    from urllib  import quote
elif version_info[0] == 3:
    from urllib.parse import quote    

class FillContentHandler(object):

    def __init__(self):
        self.content = InitContent
        import os
        self.static_res_path = os.path.join(os.getcwd(),"../resource/static")
        print self.static_path

    def get_init_controller_file_content(self,**options):
        
        if_blog = False
        blog_name = None

        if "if_blog" in options and "blog_name" in options:
            if_blog = options["if_blog"]
            blog_name = options["blog_name"]

        handler_str = self.content['BaseHandler']
        index_handler_str = self.content['handler']
        index_handler_str = index_handler_str %("Index","Index","index","")
        
        if if_blog:
            import os
            redirect_url = os.path.join(quote(blog_name),"index.html")
            redirect_url_str = "return self.redirect(self.static_url( \"{}\" ))".format(redirect_url)
            index_handler_str = index_handler_str.format("#",redirect_url_str)   
        else:
            index_handler_str = index_handler_str.format("","" )   
        return "\n".join([handler_str,index_handler_str])

    def get_new_controller(self,name,**options):

        if_blog = False
        blog_name = None

        if "if_blog" in options and "blog_name" in options:
            if_blog = options["if_blog"]
            blog_name = options["blog_name"]


        # ensuer str 's first char is upper
        controller_name = name[0].upper() + name[1:]
        controller_name_low = name[0].lower() + name[1:]

        controller__str = self.content['handler']
        controller__str = controller__str % (controller_name,controller_name,controller_name_low,controller_name_low)

        if if_blog:
            import os
            redirect_url = os.path.join(quote(blog_name),"index.html")
            redirect_url_str = "return self.redirect(self.static_url( \"{}\" ))".format(redirect_url)
            controller__str = controller__str.format("#", redirect_url_str)   
        else:
            controller__str = controller__str.format("","" )   

        return controller__str

    def get_init_setting_content(self):
        setting_str = self.content['setting']
        return setting_str


    def get_html_content(self,html_name,**options):
        def _fill_args(string,*args):
            try:
                print args
                new_str = string % tuple( args)
                return new_str
            except TypeError:
                args = list(args) + [args[0]]
            
                new_args = tuple(args )
                return _fill_args(string,*new_args)
        if "theme" in options:
            html_str = self.content[options["theme"]]
            try:
                html_str = _fill_args(html_str,html_name) 

                return html_str
            except ValueError:
                return html_str
        if "extends" in options:
            html_str = self.content["extends_html"]
            try:
                html_str = _fill_args(html_str,options['extends'])
                html_str = html_str.replace(r'$', r'%')
                return html_str
            except ValueError:
                return html_str

        html_str = self.content['html']
        html_str = _fill_args(html_str ,html_name)
        html_str = html_str.replace(r'$', r'%')
        return html_str

    def get_css_content(self,css_name):
        css_str = self.content['css']
        css_str = css_str % (css_name)
        return css_str

    def get_main_content(self):
        return self.content['main']

    def get_js_content(self,name,**options):
        js_str = self.content['js']
        js_str = js_str % (name)
        return js_str


if __name__ == '__main__':
    test = FillContentHandler()
    print test.get_init_controller_file_content()
    print test.get_init_setting_content()
    print test.get_html_content("test")
    
